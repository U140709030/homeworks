public class TriangleClassifier {

    public static void main(String args[]){

       int a=Integer.parseInt(args[0]);
       int b=Integer.parseInt(args[1]);
       int c=Integer.parseInt(args[2]);

       if(a==b && b==c)
            System.out.println("Triangle type is Equilateral Triangle");

        else if(a >= (b+c) || c >= (b+a) || b >= (a+c) )
            System.out.println("Not a Triangle");

        else if ((a==b && b!=c ) || (a!=b && c==a) || (c==b && c!=a))
            System.out.println("Triangle type is Isosceles Triangle");

	else if (a*a + b*b == c*c)
            System.out.println("Triangle type is Right Triangle");

	else if (a*a + c*c == b*b)
            System.out.println("Triangle type is Right Triangle");

	else if (c*c + b*b == a*a)
            System.out.println("Triangle type is Right Triangle");

        else if(a!=b && b!=c && c!=a)
            System.out.println("Triangle type is Scalene Triangle");
    }
}
